import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../../../shared/shared.module";
import { FormTemplateModule } from "utilscore-ar";
import { PersonService } from "../person.service";
import { PersonFormComponent } from "./person-form.component";

@NgModule({
  imports: [CommonModule, FormsModule, SharedModule, FormTemplateModule],
  declarations: [PersonFormComponent],
  exports: [PersonFormComponent],
  providers: [PersonService]
})
export class PersonFormModule {}
