import { Component } from "@angular/core";
import { PersonService } from "../person.service";
import { FormManager } from "prime-ngx-ar";
import { EIdentificationType } from "../../identificationtype/identificationtype.model";

@Component({
  selector: "person-form",
  templateUrl: "./person-form.component.html"
})
export class PersonFormComponent extends FormManager {
  TYPES: any = EIdentificationType;

  constructor(public service: PersonService) {
    super(service);
  }
}
