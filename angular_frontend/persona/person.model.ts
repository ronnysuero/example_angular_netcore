import { IPerson } from "./person.interface";
import { Guid } from "utilscore-ar";

export class PersonModel implements IPerson {
  IdentificationNumber: string;
  PersonId: Guid;
  Names: string;
  LastNames: string;
  FullName: string;
  IdentificationTypeId: number;
  IdentificacionTypeDescription: string;
  Phone: string;
  Email: string;
  Cellphone: string;
  UIN: string;
  FIN: Date;
  UUP: string;
  FUP: Date;
  UniqueIdentifier: string;
}
