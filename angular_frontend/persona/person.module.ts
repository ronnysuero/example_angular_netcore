import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { PersonListComponent } from "./list/person-list.component";
import { PersonComponent } from "./person.component";
import { PersonRouting } from "./person.routing";
import { SharedModule } from "../../shared/shared.module";
import { PersonFormModule } from "./form/person-form.module";
import { ModalFormModule } from "prime-ngx-ar";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PersonRouting,
    PersonFormModule,
    SharedModule,
    ModalFormModule
  ],
  declarations: [PersonComponent, PersonListComponent]
})
export class PersonModule {}
