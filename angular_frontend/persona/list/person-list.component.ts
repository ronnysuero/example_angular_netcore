import { Component } from "@angular/core";
import { PersonService } from "../person.service";
import { ListManager } from "prime-ngx-ar";

@Component({
  selector: "person-list",
  templateUrl: "./person-list.component.html"
})
export class PersonListComponent extends ListManager {
  constructor(public service: PersonService) {
    super(service);
  }
}
