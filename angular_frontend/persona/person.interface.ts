import { Guid } from "utilscore-ar";

export interface IPerson {
  PersonId: Guid;
  Names: string;
  Lastnames: string;
  IdentificationNumber: string;
  IdentificationTypeId?: number;
  Phone: string;
  Cellphone: string;
  Email: string;
}
