import { Routes, RouterModule } from "@angular/router";
import { PersonComponent } from "./person.component";
import { PersonListComponent } from "./list/person-list.component";
import { PersonFormComponent } from "./form/person-form.component";

const APP_ROUTES: Routes = [
  {
    path: "",
    component: PersonComponent,
    children: [
      { path: "", component: PersonListComponent },
      {
        path: "edit",
        component: PersonFormComponent,
        data: { title: "Edit" }
      },
      {
        path: "add",
        component: PersonFormComponent,
        data: { title: "Add" }
      }
    ]
  }
];

export const PersonRouting = RouterModule.forChild(APP_ROUTES);
