import { Injectable } from "@angular/core";
import { PersonModel } from "./person.model";
import { ServiceManager, DataType } from "utilscore-ar";

@Injectable({ providedIn: "root" })
export class PersonService extends ServiceManager<PersonModel> {
  constructor() {
    super("api/person", PersonModel);

    this.setOptions({
      loadingOnAdd: false,
      identityPropertyName: "PersonId",
      identityPropertyType: DataType.Guid,
      cacheDataProvider: {
        providers: ["identificacionTypes"]
      }
    });
  }
}
