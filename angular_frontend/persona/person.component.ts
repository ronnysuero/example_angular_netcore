import { Component, OnInit } from "@angular/core";
import { ComponentManager, ModalFormService } from "prime-ngx-ar";
import { PersonModel } from "./person.model";
import { PersonService } from "./person.service";
import { PersonFormComponent } from "./form/person-form.component";

@Component({
  selector: "person",
  templateUrl: "./person.component.html"
})
export class PersonComponent extends ComponentManager<PersonModel>
  implements OnInit {

  constructor(
    public service: PersonService,
    public modalFormService: ModalFormService
  ) {
    // para el servicio a la clase base y el routerLink
    super(service);
  }

  ngOnInit() {
    super.ngOnInit();
    this.setModalContent(PersonFormComponent);
  }
}
