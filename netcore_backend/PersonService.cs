using System;
using System.Collections;
using System.Linq;
using System.Linq.Expressions;
using Erp.Common.Interfaces;
using Erp.Common.Utils;
using Erp.Entity.Comunes;
using Erp.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using UtilsCore.EFCore;
using UtilsCore.Filter.Json;
using UtilsCore.Helper;
using UtilsCore.Interfaces;
using UtilsCore.Pagination;
using UtilsCore.Pagination.Interfaces;
using UtilsCore.Sort;

// ReSharper disable once CheckNamespace
namespace Erp.Service
{
    public class PersonService : IGlobalConfig, ICrudRepository<Person>
    {
        private readonly EntityDbContext _db;

        public PersonService(EntityDbContext context)
        {
            _db = context;
            GlobalConfig = new GlobalConfig();
        }

        public Person Requery(IdentityValue identity)
        {
            return Find(f => f.PersonId == new Guid(identity.ToString()));
        }

        public Person Save(Person model)
        {
            if (
                _db.Person.Any(a =>
                    a.PersonId != model.PersonId && 
                    a.IdentificationNumber == model.IdentificationNumber
                )
            )
                throw new ApplicationException("This person is already registered in database");

            model.IdentificationNumber = model.IdentificationNumber.ToUpper();

            if (model.PersonId == Guid.Empty)
            {
                model.PersonId = new Guid();
                _db.Person.Add(model);
            }
            else
            {
                _db.Entry(model).State = EntityState.Modified;
            }

            _db.SaveChanges();

            return Find(f => f.PersonId == model.PersonId);
        }

        public Person Find(Expression<Func<Person, bool>> predicate)
        {
            return Query().Where(predicate).FirstOrDefault();
        }

        public bool Delete(IdentityValue identity)
        {
            var entity = _db.Person.FirstOrDefault(f =>
                f.PersonId == new Guid(identity.ToString())
            );

            if (entity == null) throw new ApplicationException("Record not found");

            _db.Entry(entity).State = EntityState.Deleted;

            return _db.SaveChanges() > 0;
        }

        public IList GetPage(IPageParameter input)
        {
            return Query()
                .AddJsonFilters(input.Filters)
                .AddOrderByRange(input.Sorts)
                .ToPagedList(input.Page);
        }

        public IQueryable<Person> Query()
        {
            return from a in _db.Person
                join b in _db.IdentificationType on a.IdentificationTypeId equals b.IdentificationTypeId
                select new Person
                {
                    PersonId = a.PersonId,
                    Names = a.Names,
                    LastNames = a.LastNames,
                    Phone = a.Phone,
                    Email = a.Email,
                    Cellphone = a.Cellphone,
                    FullName = a.FullName,
                    IdentificationTypeId = a.IdentificationTypeId,
                    IdentificacionNumber = a.IdentificacionNumber,
                    IdentificationTypeDescripcion = b.Description,
                    UIN = a.UIN,
                    FIN = a.FIN,
                    UUP = a.UUP,
                    FUP = a.FUP,
                    UniqueIdentifier = a.UniqueIdentifier
                };
        }

        public GlobalConfig GlobalConfig { get; set; }
    }
}