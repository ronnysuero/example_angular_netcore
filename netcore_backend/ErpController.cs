﻿using Security.Core.Controller.Crud;
using UtilsCore.Interfaces;

namespace Erp.Backend.Controllers
{
    public class ErpController<TModel, TService> : CrudController<TModel, TService>
        where TService : ICrudRepository<TModel>
        where TModel : class
    {
    }
}