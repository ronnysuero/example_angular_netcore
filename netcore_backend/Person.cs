using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Erp.Entity.Clientes;
using Erp.Entity.Common;
using Newtonsoft.Json;
using UtilsCore.Attributes;
using UtilsCore.Interfaces;

namespace Erp.Entity.Comunes
{
    [Table("CMNPerson")]
    [FormName("PERSON")]
    public class Person : IUserInfo, IPerson
    {
        public Person()
        {
            PersonId = Guid.Empty;
            Providers = new HashSet<Provider>();
            ProviderContacts = new HashSet<ProviderContact>();
            Clients = new HashSet<Client>();
        }

        [NotMapped] public string IdentificationTypeDescription { get; set; }

        #region ENTITY

        [Key] public Guid PersonId { get; set; }

        [Required] [StringLength(50)] public string Names { get; set; }

        [StringLength(50)] public string LastNames { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [StringLength(100)]
        public string FullName { get; set; }

        [StringLength(15)] public string IdentificationNumber { get; set; }

        [StringLength(20)] public string Phone { get; set; }

        [StringLength(20)] public string Cellphone { get; set; }

        [StringLength(20)] public string Email { get; set; }

        public short IdentificationTypeId { get; set; }

        [JsonIgnore] public IdentificationType IdentificationType { get; set; }

        [JsonIgnore] public ICollection<Provider> Providers { get; set; }

        [JsonIgnore] public ICollection<ProviderContact> ProvidersContact { get; set; }

        [JsonIgnore] public ICollection<Client> Clients { get; set; }

        [StringLength(20)] public string UniqueIdentifier { get; set; }

        [StringLength(20)] public string UIN { get; set; }

        [Column(TypeName = "datetime")] public DateTime? FIN { get; set; }

        [StringLength(20)] public string UUP { get; set; }

        [Column(TypeName = "datetime")] public DateTime? FUP { get; set; }

        #endregion
    }

    public interface IPerson
    {
        Guid PersonId { get; set; }

        string Names { get; set; }

        string LastNames { get; set; }

        string IdentificationNumber { get; set; }

        short IdentificationTypeId { get; set; }

        string Phone { get; set; }

        string Cellphone { get; set; }

        string Email { get; set; }
    }
}