# Example CRUD made with Angular and NetCore 2.1 WebAPI

This example was built using my libraries, details see:

[prime-ngx-ar](https://www.npmjs.com/package/prime-ngx-ar)
[utilscore-ar](https://www.npmjs.com/package/utilscore-ar)

[Security.Core.Controller](https://www.nuget.org/packages/Security.Core.Controller)
[UtilsCore ](https://www.nuget.org/packages/UtilsCore)

Contact me:
[Ronny Zapata](mailto:ronnysuero@gmail.com)
